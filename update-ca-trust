#!/bin/sh

#set -vx
set -eu

# For backwards compatibility reasons, future versions of this script must
# support the syntax "update-ca-trust extract" trigger the generation of output
# files in $DEST.

DEST=/etc/pki/ca-trust/extracted

# Prevent p11-kit from reading user configuration files.
export P11_KIT_NO_USER_CONFIG=1

usage() {
	fold -s -w 76 >&2 <<-EOF
		Usage: $0 [extract] [-o DIR|--output=DIR]

		Update the system trust store in $DEST.

		COMMANDS
		(absent/empty command): Same as the extract command without arguments.

		extract: Instruct update-ca-trust to scan the source configuration in
		/usr/share/pki/ca-trust-source and /etc/pki/ca-trust/source and produce
		updated versions of the consolidated configuration files stored below
		the $DEST directory hierarchy.

		EXTRACT OPTIONS
		-o DIR, --output=DIR: Write the extracted trust store into the given
		directory instead of updating $DEST.
	EOF
}

extract() {
	USER_DEST=

        # can't use getopt here. ca-certificates can't depend on a lot
        # of other libraries since openssl depends on ca-certificates
        # just fail when we hand parse

        while [ $# -ne 0 ]; do
	    case "$1" in
	      "-o"|"--output")
		  if [ $# -lt 2 ]; then
			  echo >&2 "Error: missing argument for '$1' option. See 'update-ca-trust --help' for usage."
			  echo >&2
			  exit 1
		  fi
	          USER_DEST=$2
		  shift 2
		  continue
		  ;;
		"--")
		  shift
		  break
		  ;;
		*)
		  echo >&2 "Error: unknown extract argument '$1'. See 'update-ca-trust --help' for usage."
		  exit 1
		  ;;
	    esac
	done

	if [ -n "$USER_DEST" ]; then
		DEST=$USER_DEST
	        # Attempt to create the directories if they do not exist
                # yet (rhbz#2241240)
	        /usr/bin/mkdir -p \
		    "$DEST"/openssl \
		    "$DEST"/pem \
		    "$DEST"/java \
		    "$DEST"/edk2
	fi

	# OpenSSL PEM bundle that includes trust flags
	# (BEGIN TRUSTED CERTIFICATE)
	/usr/bin/trust extract --format=openssl-bundle --filter=certificates --overwrite --comment "$DEST/openssl/ca-bundle.trust.crt"
	/usr/bin/trust extract --format=pem-bundle --filter=ca-anchors --overwrite --comment --purpose server-auth "$DEST/pem/tls-ca-bundle.pem"
	/usr/bin/trust extract --format=pem-bundle --filter=ca-anchors --overwrite --comment --purpose email "$DEST/pem/email-ca-bundle.pem"
	/usr/bin/trust extract --format=pem-bundle --filter=ca-anchors --overwrite --comment --purpose code-signing "$DEST/pem/objsign-ca-bundle.pem"
	/usr/bin/trust extract --format=java-cacerts --filter=ca-anchors --overwrite --purpose server-auth "$DEST/java/cacerts"
	/usr/bin/trust extract --format=edk2-cacerts --filter=ca-anchors --overwrite --purpose=server-auth "$DEST/edk2/cacerts.bin"
	# Hashed directory of BEGIN TRUSTED-style certs (usable as OpenSSL CApath and
	# by GnuTLS)
	/usr/bin/trust extract --format=pem-directory-hash --filter=ca-anchors --overwrite --purpose server-auth "$DEST/pem/directory-hash"

	# p11-kit extract will have made this directory unwritable; when run with
	# CAP_DAC_OVERRIDE this does not matter, but in container use cases that may
	# not be the case. See rhbz#2241240.
	if [ -n "$USER_DEST" ]; then
	    /usr/bin/chmod u+w "$DEST/pem/directory-hash"
        fi

	# Debian compatibility: their /etc/ssl/certs has this bundle
	/usr/bin/ln -s ../tls-ca-bundle.pem "$DEST/pem/directory-hash/ca-certificates.crt"
	# Backwards compatibility: RHEL/Fedora provided a /etc/ssl/certs/ca-bundle.crt
	# since https://bugzilla.redhat.com/show_bug.cgi?id=572725
	/usr/bin/ln -s ../tls-ca-bundle.pem "$DEST/pem/directory-hash/ca-bundle.crt"

	# Remove write permissions again
	if [ -n "$USER_DEST" ]; then
	    /usr/bin/chmod u-w "$DEST/pem/directory-hash"
        fi
}

if [ $# -lt 1 ]; then
    set -- extract
fi

case "$1" in
	"extract")
		shift
		extract "$@"
	;;
	"--help")
		usage
		exit 0
	;;
	"-o"|"--output")
		echo >&2 "Error: the '$1' option must be preceded with the 'extract' command. See 'update-ca-trust --help' for usage."
		echo >&2
		exit 1
	;;
	"enable")
		echo >&2 "Warning: 'enable' is a deprecated argument. Use 'update-ca-trust extract' in future. See 'update-ca-trust --help' for usage."
		echo >&2
		echo >&2 "Proceeding with extraction anyway for backwards compatibility."
		extract
	;;
	*)
		echo >&2 "Warning: unknown command: '$1', see 'update-ca-trust --help' for usage."
		echo >&2
		echo >&2 "Proceeding with extraction anyway for backwards compatibility."
		extract
	;;
esac
